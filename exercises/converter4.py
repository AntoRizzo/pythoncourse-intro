#!/usr/bin/env python

"""
Exercise converter4
--------------------

Do the same as you did in exercise **converter1** but using the numpy module.
Work with the data in the form of numpy.arrays

Tips:

- you can save typing by doing: "import numpy as np" (you could even do
  "from numpy import *" but it is not recommended because it would pollute
  your namespace and make debugging more difficult)
- Use numpy.loadtxt() for reading and and numpy.savetxt() for writing
- You will find the following functions and methods very useful: numpy.max(),
  numpy.arange() numpy.zeros() and numpy.array.flatten()
- Remember that arithmetical operations on numpy arrays are element-wise
  (you do not need any explicit loop in this exercise!)
- [Official Solution](exercises/converter4.py)
"""

# Write your solution here
